import React, {useState} from 'react';
import axios from 'axios';
import { API_URL } from '../../config';
import { useAuth } from "../../context/user";
import {withRouter} from "react-router-dom";

import LoginForm from '../../components/LoginForm/LoginForm';


const Login = ({history}) => {

    const { setAuthToken, tokenError, setTokenError} = useAuth();
    const [loginError, setError] = useState(false);

    const handleLogin = (username, password) => {
        axios.post(`${API_URL}/login`,{ "username": username, "password": password })
        .then((res) => {
          if (res.status === 200) {
            setAuthToken(res.data.token);
            setError(false);
            if(tokenError) {
                setTokenError(false)
            }
            history.push('/')
          }
        })
        .catch((error) => {
            if(tokenError) {
                setTokenError(false)
            }
            setError(error.response.data);
        });
    }

    let error = tokenError ? tokenError : loginError ? loginError : null;

    return (
    <div className="Login">
       <LoginForm handleSubmit={handleLogin} error={error} />
    </div>
    )
}

export default withRouter(Login);
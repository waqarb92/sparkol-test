import React from 'react';
import {withRouter} from 'react-router-dom';
import { useAuth } from "../../context/user";

import Header from '../../components/Header/Header';
import Welcome from '../../components/Welcome/Welcome';

const Home = ({history}) => {
    const { setAuthToken, User } = useAuth();

    const handleLogout = () => {
        setAuthToken(false);
        history.push('/login');
    }

    return (
        <div>
        <Header Logout={() => handleLogout()} />
        <Welcome user={User} />
        </div>
    );
}

export default withRouter(Home)
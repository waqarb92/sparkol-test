import React from 'react';
import PropTypes from 'prop-types';

import './Welcome.scss';


const Welcome = ({user}) => {

    return (
        <div className="Container Welcome">
            <h1 className="Welcome__Title">Hello {user.name}</h1>
            <p className="Welcome__Content">This is the logged in page, sadly there is nothing else to do here yet.</p>
        </div>
    )
}

Welcome.propTypes = {
    user: PropTypes.object.isRequired,
}

export default Welcome;
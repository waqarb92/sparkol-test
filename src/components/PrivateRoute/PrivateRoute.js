import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useAuth } from "../../context/user";


const PrivateRoute = ({component: Component, ...rest}) => {
    const { User, isLoaded } = useAuth();

    return (
        <Route {...rest} render={props => 
            !isLoaded ? (
                <>
                </>
            ) :
            User ? (
                <Component {...props} />
            )
            : (
            <Redirect to="/login" />
        )} />
    );
};

export default PrivateRoute;
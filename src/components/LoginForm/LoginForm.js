import React, {useState} from 'react';
import PropTypes from 'prop-types';

import './LoginForm.scss';


const LoginForm = ({handleSubmit, error}) => {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const submitForm = e => {
        e.preventDefault();
        handleSubmit(username, password);

    }

    return (
        <form className="Container LF" onSubmit={(e) => submitForm(e)}>
            <h1 className="LF__Title">Sparkol Assessment</h1>
            <h2 className="LF__Subtitle">Please login to continue</h2> 
            <label className="LF__input">
            <span>Username</span>
            <input type="text" value={username} name="Username" onChange={(e) => setUsername(e.target.value)} />
            </label>
            <label className="LF__input">
            <span>Password</span>
            <input type="password" value={password} name="Password" onChange={(e) => setPassword(e.target.value)} />
            </label>
            <span className="LF__Error">{error}</span>
            <input className="primary-btn" type="submit" value="LOG IN TO CONTINUE" />
        </form>
    )
}

LoginForm.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    error: PropTypes.string
}

LoginForm.defaultProps = {
    error: ''
}

export default LoginForm;
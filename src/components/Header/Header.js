import React from 'react';
import PropTypes from 'prop-types';

import './Header.scss';


const Header = ({Logout}) => {

    return (
        <div className="Header">
            <button type="button" className="Header__logout primary-btn" onClick={() => Logout()}>LOG OUT</button>
        </div>
    )
}

Header.propTypes = {
    Logout: PropTypes.func.isRequired,
}

export default Header;
module.exports = {
    AUTH_SECRET: process.env.AUTH_SECRET || 'development',
    API_URL: process.env.API_URL || 'http://localhost:3333'
  }
  
import React, {useState, useEffect} from 'react';
import { AuthContext } from "./context/user";
import { AUTH_SECRET, API_URL } from './config';
import axios from 'axios';
import jwt from 'jsonwebtoken';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Home from './pages/Home/Home';
import Login from './pages/Login/Login';
import PrivateRoute from './components/PrivateRoute/PrivateRoute';


import './App.scss';

const App = () => {
  const existingToken = JSON.parse(localStorage.getItem("User"));

  const [Token, setAuthToken] = useState(existingToken);
  const [tokenError, setTokenError] = useState(false);
  const [isLoaded, setLoaded] = useState(false);
  const [User, setUser] = useState(false);

  useEffect(() => {
    if (existingToken) {
      axios.get(`${API_URL}/verifyToken`, {
            headers: {
              'Authorization': `Bearer ${existingToken}`
            }
          })
          .then((res) => {
            if(res.status === 200) {
              const decoded = jwt.decode(existingToken, AUTH_SECRET);
              setUser(decoded.user);
              setLoaded(true);
            }
          })
          .catch((error) => {
            setTokenError(error.response.data);
            setLoaded(true);
          })
    } else {
      setLoaded(true);
    }
  }, [existingToken]);

  const decodeToken = data => {
    const decoded = jwt.decode(data, AUTH_SECRET);
    return decoded.user;
  }
  
  const setTokens = (data) => {
    if(data) {
      localStorage.setItem("User", JSON.stringify(data));
      setUser(decodeToken(data));
      setAuthToken(data);
    } else {
      localStorage.removeItem("User");
      setUser(null);
      setAuthToken(null);

    }

  }


    return (
      <AuthContext.Provider value={{ Token, setAuthToken: setTokens, User, tokenError, setTokenError, isLoaded }}>
      <Router>
          <Switch>
            <PrivateRoute component={Home} path="/" exact />
            <Route exact path="/login" component={Login} />
          </Switch>
      </Router>
      </AuthContext.Provider>
    );
  }

export default App;
